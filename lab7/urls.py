from django.urls import path
from .views import index, add_status

urlpatterns = [
    path('', index, name="homepage"),
    path('add_status/', add_status, name='add_status'),
]
