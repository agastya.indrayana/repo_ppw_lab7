from django.test import TestCase, Client
from django.urls import resolve
from .models import Status
from .views import index, add_status
from .forms import statusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from datetime import datetime
import pytz


class lab7Test(TestCase):
    def test_lab_7_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_lab_7_using_homepage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'lab7/homepage.html')

    def test_lab_7_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_lab_7_landing_page_greating_exist(self):
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa kabar?', html_response)

    def test_model_can_create_new_status(self):
        #Creating a new status
        dateTime = datetime.now()
        Status.objects.create(dateTime = dateTime.replace(tzinfo=pytz.UTC), status="hello world")

        #Retrieving all available status
        counting_all_available_message= Status.objects.all().count()
        self.assertEqual(counting_all_available_message,1)

    def test_can_save_a_POST_request(self):
        response = self.client.post('/add_status/', data={'status' : 'hello world'})
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('hello world', html_response)

    def test_form_message_input_has_placeholder_and_css_classes(self):
        form = statusForm()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('<label for="id_status">Status:</label>', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = statusForm(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )


class lab7FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(lab7FunctionalTest,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(lab7FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        status_field = selenium.find_element_by_id('id_status')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        status_field.send_keys('hello world')

        # submitting the form
        submit.send_keys(Keys.RETURN)

        assert "hello world" in selenium.page_source

    def test_homepage_title(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        assert "Homepage" in selenium.title

    def test_homepage_has_navbar(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        navbar = selenium.find_elements_by_tag_name("nav")
        assert len(navbar) > 0

    def test_form_uses_css(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        status_field = selenium.find_element_by_id('id_status')

        assert "form-control" in status_field.get_attribute("class")

    def test_navbar_uses_css(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        navbar = selenium.find_element_by_tag_name("nav")

        assert "navbar-inverse" in navbar.get_attribute("class")
